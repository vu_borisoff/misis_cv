#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

using namespace std;
using namespace cv;

const int PIXELS = 255;
const int TOP_PADDING = 10;
const int THICKNESS = 3;
const int SHIFT = 2;

vector <int> count_pixel(PIXELS);


///подсчитаем количество пикселей со значениеями [0.. 255] на изображении
///img: одноканальное изображение
void calc_pixels (const Mat &img)
{
    for (int i = 0; i < img.rows; i++)
        for (int j = 0; j < img.cols; j++)
        {
            Vec3b tmp = img.at<int>(i, j);
            count_pixel[tmp[0]]++;
        }
}

Mat get_hist (const Mat& src)
{
    Mat hist(PIXELS + TOP_PADDING, PIXELS * (THICKNESS + SHIFT), CV_8UC1);


    calc_pixels(src);

    normalize(count_pixel, count_pixel, 0, hist.rows, NORM_MINMAX, -1);


    int col_on_image = 0;
    for (int i = 0; i < PIXELS; i++)
    {

        for (int j = 0; j < THICKNESS; j++, col_on_image++)
        {
            Point up(col_on_image, count_pixel[i] + TOP_PADDING);
            Point down(col_on_image, hist.rows);
            line(hist, down, up, Scalar(255,0,0));
        }
        for (int j = 0; j < SHIFT; j++, col_on_image++) {}
    }
    return hist;
}



int main ()
{

    bool color_img {true};
    Mat img;
    string image_path;

    Mat channels[3];

    if (!color_img)
    {
        image_path = "../../Images/cat.jpeg";
        img = imread(image_path, IMREAD_GRAYSCALE);
        imshow("Source image", img);
        imshow("Histogram", get_hist(img));
    }
    else
    {
        image_path = "../../Images/car.jpeg";
        img = imread(image_path, IMREAD_COLOR);
        split(img, channels);
        imshow("Source image", img);
        imshow("Blue Histogram", get_hist(channels[0]));
        imshow("Green Histogram", get_hist(channels[1]));
        imshow("Red Histogram", get_hist(channels[2]));
    }

    waitKey(0);



}
