cmake_minimum_required(VERSION 3.8)
project(Task2)

add_executable(Task2 task2_main.cpp)

set_property(TARGET Task2 PROPERTY CXX_STANDARD 11)

include_directories(${OpenCV_DIRS})

target_link_libraries(Task2
        ${OpenCV_LIBS}
        )