#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

using namespace std;
using namespace cv;

int square_size = 150;

int rows = 2;
int cols = 3;


double get_mean (Mat src)
{
    double ans = 0;

    for (int i = 0; i < src.rows; i++)
        for (int j = 0; j < src.cols; j++)
            ans += src.at<uint8_t>(i, j);

    return (ans / src.rows) / src.cols;
}

int main ()
{
    Mat src(Size(450, 300), CV_8UC1, Scalar(0, 0, 0));

    Scalar black(0, 0, 0);
    Scalar grey(127, 127, 127);
    Scalar white(255, 255, 255);

    vector <vector <Scalar> > sqr_colors = { {grey, black, white}, {black, white, grey}};
    vector <vector <Scalar> > circle_colors = { {black, white, grey}, {grey, black, white}};

    for (int i = 0; i < rows; ++i)
    {
        for (int j = 0; j < cols; ++j)
        {
            int x = (j) * square_size;
            int y = (i) * square_size;

            int x_c = x + square_size / 2;
            int y_c = y + square_size / 2;

            rectangle(src, Rect(x, y, square_size, square_size), sqr_colors[i][j], CV_FILLED);
            circle(src, Point(x_c, y_c), square_size / 2, circle_colors[i][j], CV_FILLED);
        }
    }

    imshow("src", src);

    Mat kernel(2, 2, CV_32FC1);
    kernel.at<float>(0, 0) = -1;
    kernel.at<float>(0, 1) = 0;
    kernel.at<float>(1, 0) = 0;
    kernel.at<float>(1, 1) = 1;

    Mat kernel2(2, 2, CV_32FC1);
    kernel2.at<float>(0, 0) = 0;
    kernel2.at<float>(0, 1) = 1;
    kernel2.at<float>(1, 0) = -1;
    kernel2.at<float>(1, 1) = 0;


    Mat out(src.clone());
    Mat out2(src.clone());

    filter2D(src,out, -1, kernel, Point(-1, -1), 128, BORDER_REPLICATE);
    filter2D(src,out2, -1, kernel2, Point(-1, -1), 128, BORDER_REPLICATE);


    imshow("out", out);
    imshow("out2", out2);


    cout << "SQRT(MEAN^2(OUT)  - MEAN^2(SRC)): " << sqrtf(get_mean(out) * get_mean(out)- get_mean(src) * get_mean(src)) << endl;
    cout << "SQRT(MEAN^2(OUT2) - MEAN^2(SRC)): " << sqrtf(get_mean(out2) * get_mean(out2)- get_mean(src) * get_mean(src)) << endl;
    cout << "SQRT(MEAN^2(OUT2) - MEAN^2(OUT)): " << sqrtf(get_mean(out2) * get_mean(out2)- get_mean(out) * get_mean(out)) << endl;


    waitKey(0);
}