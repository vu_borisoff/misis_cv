#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>


using namespace cv;
using namespace std;


const double MAX_VAL = 100;
Mat inp, out;


uchar fun (double x, double a)
{
	if (a == 0)
		return x;
	else
		return (uchar)max(0.0, min(255.0, ((x * x + 2 * x * a + a * a) / 2) / (x - a)));
}

void trackbar_callback (int value, void *user_data)
{
    for (int i = 0; i < inp.rows; i++)
    {
        for (int j = 0; j < inp.cols; j++)
        {
			out.at<uchar>(i, j) = fun(inp.at<uchar>(i, j), value);
        }
    }
}

Mat res;
int cur_pos = 0;

int main ()
{

    inp = imread("../../Images/cat.jpeg", CV_8UC1);
    out = inp.clone();

    namedWindow("Track_bar", CV_WINDOW_NORMAL);

  
    createTrackbar("parameter", "Track_bar", &cur_pos, MAX_VAL, trackbar_callback);


    while (1)
    {
        hconcat(inp, out, res);
        imshow("Track_bar", res);
        int key = waitKey(30);
        if (key == ' ')
            break;

    }
}