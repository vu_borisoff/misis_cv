#include <detector.h>
#include <detector_retina.h>

using namespace std;
using namespace cv;



int main()
{

	string video_name = "fireshow.mov";
	//string video_name = "motocross.mp4";

	string path_to_video = "../../Videos/" + video_name;
	VideoCapture video(path_to_video);

	Mat cur_frame, motion_res_frame, retina_res_frame;

	while (video.isOpened() && cur_frame.empty())
		video >> cur_frame;
	

	RetinaDetector retina (cur_frame);
	MotionDetector motion (cur_frame);

	vector <Rect2f> bboxes_retina;

	while (video.isOpened())
	{
		video >> cur_frame;
		if (cur_frame.empty())
			continue;

		retina_res_frame = cur_frame.clone();
		motion_res_frame = cur_frame.clone();

		retina.update(cur_frame, bboxes_retina);
		motion.detect(cur_frame, motion_res_frame);

		///������ �������� ��������� �������������� � ���������
		if (0 < bboxes_retina.size() && bboxes_retina.size() < 10)
		{
			for (int i = 0; i < bboxes_retina.size(); i++)
			{
				cv::rectangle(retina_res_frame, bboxes_retina[i], cv::Scalar(255, 0, 0), 1, 8, 0);
			}

		}

		namedWindow("Retina", CV_WINDOW_NORMAL);
		namedWindow("Motion", CV_WINDOW_NORMAL);
		imshow("Retina", retina_res_frame);
		imshow("Motion", motion_res_frame);
		bboxes_retina.clear();
		waitKey(20);
	}

	return 0;
}