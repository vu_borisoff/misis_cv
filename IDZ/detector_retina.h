#ifndef DETECTOR_RETINA_HG
#define DETECTOR_RETINA_HG

#include <iostream>
#include <vector>
#include <deque>
#include "opencv2/bioinspired.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/core/ocl.hpp"


///��� ���������������/����������� ������ ��������� retina
#define RETINA_DEBUG

class RetinaDetector
{
public:
	///-------�������� ����������---------///
	static const short MAX_BUFFER_SIZE = 70;
	cv::Ptr<cv::bioinspired::Retina> cvRetina;                   // ���������� ��� ������ � retina.                                     
	int numFrame = 0;		                                 	 // ���������� ������ ����������� � �������
	std::deque<double> mean_buffer;								 // ����� �������� ������� ������� ��� ��������� MAX_BUFFER_SIZE �����������
	double lower_sensitive = 0.98;								 // ����������������, ���������� �������� �� 0 �� 1. ������ �������� - ������ ����������������
	double upper_sensetive = 0.05;								 // ������� ������� ����������������, ������ �������� - ������ ���������������
	int threshold_min = 100;									 // ����������� ������������� �������� ��� ����������� �������� ��������
	double min_area = 500;
															 
	///----------------------------------///

    ///-------�������� ������ � �������------////
	RetinaDetector(const cv::Mat &inputFrame, const std::string &settings_path = "RetinaDefaultParameters.xml"); // �������������
	RetinaDetector() {} // just debug

	// ����� ��� ���������� ��������� ������� ������
	//  inputFrame - ������� ����������� RGB ���� CV_8UC3
	//  arrayBB - ������ ��������������� �����
	void update(cv::Mat inputFrame, std::vector<cv::Rect2f>& arrayBB);

	// ���������� ������� ������� 
	// mean_ - ��� ������� �������� �������� �� ��������� ������������
	double get_median_of_array(const std::deque<double> &mean_);
	///----------------------------------///
};




#endif DETECTOR_RETINA_HG
