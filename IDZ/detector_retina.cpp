#include "detector_retina.h"

using namespace std;
using namespace cv;
using namespace bioinspired;

///����������� ��������� Retina
///-----------
/// ���������
/// input_frame - �������� ��� ��������� ��������� ���������, �� ��� �� ���� ����� ������ ������
/// settings_path - ���� �� xml ����� � ����������� ������
///-----------
RetinaDetector::RetinaDetector(const cv::Mat &inputFrame, const string &settings_path)
{
#ifdef RETINA_DEBUG
	cout << "Init Retina" << endl;
#endif
	cvRetina = cv::bioinspired::Retina::create(
		inputFrame.size(), // ������������� ������ �����������
		false, // ��������� ����� ���������: ��� ��������� �����
		cv::bioinspired::RETINA_COLOR_BAYER, // ��� ������� �����
		false, // ��������� ��������� ��� ���������
		1.0, // �� ������������. ���������� ����������� ���������� ��������� �����
		10.0); // �� ������������
			   // ��������� ����������� ���������

#ifdef RETINA_DEBUG
	cout << "Init End" << endl;
#endif
	///���� ������ ���������� ����, ������ ������ � ����� ������������ ������������� ���������
	///����� ���� ������ ��������� � �������
	///TODO: ��� �� ��-�������� ���� ������ ���������� � ���� ��� ������������� ���������
	cvRetina->setup(settings_path);


	// ������� �����
	cvRetina->clearBuffers();
}

double RetinaDetector::get_median_of_array(const deque<double> &mean_buffer_)
{

	///���������� ��������� ������
	if (mean_buffer_.size() == 0)
		return 0.0;
	if (mean_buffer_.size() == 1)
		return mean_buffer_.front();

	///������� ������ �� ���� � ������ ��� � ��� �������
	vector <double> tmp_mean(mean_buffer_.begin(), mean_buffer_.end());

	sort(tmp_mean.begin(), tmp_mean.end());

	///������� ������ ��������� �������� �������, ��� ��� ��� ����� ����������� ������
	int mid = tmp_mean.size() / 2;

	///�� ��������, ��� ������� � ������� ������ � �������� ������ ��������� � ������ ������
	if (tmp_mean.size() % 2 == 0)
	{
		return (tmp_mean[mid] + tmp_mean[mid + 1]) / 2.0;
	}
	else
	{
		return tmp_mean[mid + 1];
	}
};


/// �������� 
/// ������� �������� �������� �������� ������� �� ������������� �����������
///-------------------
/// ���������
/// img - ������������� ����������� 
///-------------------
/// ��������� 
/// float - ������� �������� ������� �� �����������
double calc_median(Mat img)
{
	///���� ����� ����������� �������� ���� ��������
	double res = 0;
	double total_pixels = 0;
	long long cnt = 0;
	///��������� ����� ������ ������� ��������
	for (int i = 0; i < img.rows; i++)
	{
		for (int j = 0; j < img.cols; j++)
		{
			uchar bgrPixel = img.at<uchar>(i, j);

			///����������� ������ �������, ����� ����� �� ��������� �������
			if (bgrPixel > 0)
			{
				res += bgrPixel;
				total_pixels++;
			}
		}
	}

	///�������� ������� �������� ������� �� ����� �����������
	return res / total_pixels;
}


void RetinaDetector::update(cv::Mat inputFrame, std::vector<cv::Rect2f>& arrayBB)
{

	cv::Mat retinaOutputMagno; // ����������� �� ������ magno, ����� ��� 8UC1
	cv::Mat imgTemp; // ����������� ��� ���������� ��������������
	double medianMean; // ��������������� ��������

					   ///�������� �������� ����������� ����� ������
	cvRetina->run(inputFrame);
	// ��������� ����������� ��������� ��������
	cvRetina->getMagno(retinaOutputMagno);
	// ��������� �� ������, ���� ����� ��� �������
#ifdef RETINA_DEBUG
	namedWindow("retinaOutputMagno", CV_WINDOW_NORMAL);
	cv::imshow("retinaOutputMagno", retinaOutputMagno);
#endif
	cv::waitKey(30);

	// ������� ���������� ������ �� ��� ���, ���� �� ������ ��������� �����
	if (numFrame < MAX_BUFFER_SIZE)
	{
		numFrame++;
	}


	// �������� ������� �������� ������� ���� ��������
	double mean = calc_median(retinaOutputMagno);


	///������ � ������ ��� ����������, ������� ����� ������� ������ 
	if (mean_buffer.size() >= MAX_BUFFER_SIZE)
		mean_buffer.pop_front();


	///������� � ����� ���������� ����
	mean_buffer.push_back(mean);

	///��������� ������ ������� ������� �� ��������� ������
	medianMean = get_median_of_array(mean_buffer);

#ifdef RETINA_DEBUG
	cout << "Image mean: " << mean << " Median of images " << medianMean << endl;
#endif
	///���� ������� ������� ����������� ������ ���������� �������� � �����
	///��� ������� ��� �������� ������� ������� �������� � ������ 100, �� ����� �������� �� 30 �� 50

	if (medianMean * (1.0 + upper_sensetive) < mean || mean < medianMean * lower_sensitive)
	{
#ifdef RETINA_DEBUG
		cout << "Retina DETECTED!!!" << endl;
#endif
		///������� �������� ����� ������� ��� ��� ��� ������������� ������� � ������� ���������� ��������
		cv::threshold(retinaOutputMagno, imgTemp, threshold_min, 255.0, CV_THRESH_BINARY);

#ifdef RETINA_DEBUG
		namedWindow("Threshold_Retina", CV_WINDOW_NORMAL);
		imshow("Threshold_Retina", imgTemp);
		waitKey(30);
#endif 
		// ������ �������
		std::vector<std::vector<cv::Point> > contours;
		cv::findContours(imgTemp, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

		if (contours.size() > 0)
		{
			// ������ ��������������� �����
			double xMax, yMax;
			double xMin, yMin;
			for (unsigned long i = 0; i < contours.size(); i++)
			{
				xMax = yMax = 0;
				xMin = yMin = imgTemp.cols;
				for (unsigned long z = 0; z < contours[i].size(); z++)
				{
					xMin = min(xMin, (double)contours[i][z].x);
					xMax = max(xMax, (double)contours[i][z].x);
					yMin = min(yMin, (double)contours[i][z].y);
					yMax = max(yMax, (double)contours[i][z].y);
				}
				

				if (abs(yMax - yMin) * abs(xMax - xMin) > min_area)
					arrayBB.push_back(Rect2f(xMin, yMin, abs(xMax - xMin), abs(yMax - yMin)));

			}
		}
		else
		{
			arrayBB.clear();
		}
	}
	else
	{
		arrayBB.clear();
	}
	// ��������� ������
	retinaOutputMagno.release();
	imgTemp.release();
}