#ifndef  IDZ_DETECTOR_HG
#define  IDZ_DETECTOR_HG


#include <opencv2/opencv.hpp>
#include <opencv2/core/ocl.hpp>



class MotionDetector 
{
private:
	cv::Mat firstFrame;
public:
	MotionDetector(cv::Mat frame);
	MotionDetector() {} 

	///----���������----
	/// frame - ������� ���� 
	/// result - �� ���� ����� frame ���� ����� ������ �������������� � ������ �������� 
	///----���������----
	/// 1 - �������� ��������� ����������� ��������� ����
	/// 0 - ������ ������ �� ��������� 
	/// -------------------
	bool detect(cv::Mat frame, cv::Mat& result);

	///��������� ����, ������� �������������� ��������, �� ���� ���� �������������� �������� ������� ����������
	void update(cv::Mat frame);
};
#endif // ! IDZ_DETECTOR_HG

