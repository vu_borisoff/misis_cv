#include "detector.h"

using namespace std;
using namespace cv;

MotionDetector::MotionDetector(Mat frame) 
{
	update(frame);
}


void MotionDetector::update(Mat frame) 
{
	cvtColor(frame, firstFrame, COLOR_BGR2GRAY);
	GaussianBlur(firstFrame, firstFrame, Size(21, 21), 0);
}

bool MotionDetector::detect(Mat frame, Mat& result) 
{
	Mat gray, frameDelta, thresh;
	vector<vector<Point> > cnts;
	cvtColor(frame, gray, COLOR_BGR2GRAY);
	GaussianBlur(gray, gray, Size(21, 21), 0);

	absdiff(firstFrame, gray, frameDelta);
	threshold(frameDelta, thresh, 30, 255, THRESH_BINARY);
	dilate(thresh, thresh, Mat(), Point(-1, -1), 1);
	findContours(thresh, cnts, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
	//cvtColor(frame, result, COLOR_GRAY2BGR);
	result = frame.clone();
	bool detected = false;
	for (int i = 0; i < cnts.size(); i++) 
	{
		pair<int, int> x = { frame.size().width, 0 }, y = { frame.size().height, 0 };
		for (int z = 0; z < cnts[i].size(); ++z) 
		{
			x.first =  min(cnts[i][z].x, x.first);
			x.second = max(cnts[i][z].x, x.second);
			y.first =  min(cnts[i][z].y, y.first);
			y.second = max(cnts[i][z].y, y.second);
		}
		Rect bb(x.first, y.first, x.second - x.first, y.second - y.first);
		if (bb.area() < 10) continue;
		detected = true;
		rectangle(result, bb, Scalar(0, 0, 255));
	}

	return detected;
}